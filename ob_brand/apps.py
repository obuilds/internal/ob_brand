from django.apps import AppConfig


class ObBrandConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ob_brand'
