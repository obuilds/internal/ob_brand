from setuptools import setup, find_packages


setup(
    name="ob_brand",
    version="5",
    url="https://gitlab.com/obuilds/internal/ob_brand",
    author="oBuilds Holdings, LLC",
    author_email="k@obuilds.com",
    description="Django app for oBuilds's brand",
    packages=find_packages(),
    include_package_data=True,
)
