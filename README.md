# oBuilds Brand

This is a lightweight repo, packaged as a Django app, to define oBuilds's brand. It presents Sass files and logo images as Django static files that can be used in other projects.


# Change Log

- `v5`:
  - Change to the "Yeti" Bootswatch theme as a base.
- `v4`:
  - Updated logos.
  - Color updates.
- `v3`:
  - Adds [Bootstrap Icons](https://github.com/twbs/icons).
- `v2`:
  - Brand ready for use.
- `v1`:
  - Basic app structure.
